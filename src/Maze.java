import java.util.*;

public class Maze {

    private static int[][] arr = new int[][] {
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 3, 0, 0, 0, 0, 0, 1},
            {1, 1, 0, 1, 1, 1, 0, 1},
            {1, 1, 0, 1, 0, 1, 0, 1},
            {1, 0, 0, 0, 0, 1, 0, 1},
            {1, 1, 0, 1, 1, 1, 0, 1},
            {1, 0, 0, 0, 0, 1, 2, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
    };

    private static class Point {
        int x;
        int y;
        Point parent;

        public Point(int x, int y, Point parent) {
            this.x = x;
            this.y = y;
            this.parent = parent;
        }

        public Point getParent() {
            return this.parent;
        }

        public String toString() {
            return "x = " + x + " y = " + y;
        }
    }

    private static Queue<Point> q = new LinkedList<Point>();

    private static Point getPathBFS(int x, int y) {

        q.add(new Point(x,y, null));

        while(!q.isEmpty()) {
            Point p = q.remove();

            if (arr[p.x][p.y] == 2) {
                System.out.println("Exit is reached!");
                return p;
            }

            if(isFree(p.x+1,p.y)) { // up
                arr[p.x][p.y] = +3;
                Point nextP = new Point(p.x+1,p.y, p);
                q.add(nextP);
            }

            if(isFree(p.x-1,p.y)) { // down
                arr[p.x][p.y] = +3;
                Point nextP = new Point(p.x-1,p.y, p);
                q.add(nextP);
            }

            if(isFree(p.x,p.y+1)) { // right
                arr[p.x][p.y] = +3;
                Point nextP = new Point(p.x,p.y+1, p);
                q.add(nextP);
            }

            if(isFree(p.x,p.y-1)) { // left
                arr[p.x][p.y] = +3;
                Point nextP = new Point(p.x,p.y-1, p);
                q.add(nextP);
            }

        }
        return null;
    }


    public static boolean isFree(int x, int y) {
        if((x >= 0 && x < arr.length) && (y >= 0 && y < arr[x].length) && (arr[x][y] == 0 || arr[x][y] == 2)) {
            return true;
        }
        return false;
    }

    public static void display() {
        for (int i = 0; i < 8; i++) {
            // draw the north edge
            for (int j = 0; j < 8; j++) {
                System.out.print((arr[j][i] & 1) == 1 ? "+---" : "+   ");
            }
            System.out.println("+");
            // draw the west edge
            for (int j = 0; j < 8; j++) {
                System.out.print((arr[j][i] & 8) == 0 ? "|   " : "    ");
            }
            System.out.println("|");
        }
        // draw the bottom line
        for (int j = 0; j < 8; j++) {
            System.out.print("+---");
        }
        System.out.println("+");
    }

    public static int[][] generate(int n) {

        int[][] maze = new int[n][n];
        Random rand = new Random();

        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                maze[i][j] = rand.nextInt(2);
        return maze;

    }

    public static int[][] generateMaze(int n) {

        int[][] maze = new int[n][n];
//        Random rand = new Random();
//
//        boolean start = false;
//        boolean end = false;
//
//        for (int i = 0; i < n; i++){
//            for (int j = 0; j < n; j++){
//                if(!start){
//                    start = true;
//                    maze[i][j] = 3;
//                    System.out.print(maze[i][j]);
//                } else {
//                    maze[i][j] = rand.nextInt(2);
//                    System.out.print(maze[i][j]);
//                }
//
//            }
//            System.out.println();
//        }
        return maze;
    }

    public static void printMaze(){

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
    }

    public static void printRoute(ArrayList<Point> route){

        for (Point point : route) {
            System.out.println(point);
        } System.out.println();

    }


    public static ArrayList<Point> createRoute(Point p){

        ArrayList<Point> route = new ArrayList<>();

        while(p.getParent() != null) {
            route.add(p);
            p = p.getParent();
        }
        Collections.reverse(route);

        return route;
    }


    public static void main(String[] args) {

        System.out.println();
        System.out.println("Print out maze...");

        printMaze();

        Point p = getPathBFS(1,1);

        System.out.println("Print out BFS maze...");

        printMaze();

        printRoute(createRoute(p));
    }

}
